source("libs.R")

# Set path where mail log files are stored
logpath = '/logs/'

theme1 <- theme_linedraw( ) +
    theme(
        plot.title = element_text(colour = "#000000", face = "plain"),
        plot.background = element_rect(fill = "#f5f5f5", colour = NA),
        panel.grid.major = element_line(size = 0.5, linetype = 'solid', colour = "#FCE4C4"), 
        panel.grid.minor.x = element_line(linetype = "dashed", size = 0.02 ),
        panel.grid.minor.y = element_line(NA),
        panel.grid.major.y = element_line(linetype = "dotted", size = 0.03),
        panel.border = element_blank(),
        panel.background = element_blank(),
        text = element_text(family = NA, colour = "#000000", face = "plain", size = 10),
        axis.title.x = element_blank(),
        axis.text.x = element_text(family = NA, colour = "#000000", face = "plain"),
        axis.title.y = element_blank(),
        axis.ticks = element_blank(),
        legend.title = element_blank(),
        legend.box.background = element_blank(),
        legend.margin = margin(t = 0.1, unit="cm"),
        legend.key.size = unit(1.3,"line"),
        legend.position = "right",
        legend.text.align = 1,
        legend.spacing.x = unit(1.3,"mm")
    )


sidebar <- dashboardSidebar(
    #menuItem("Monitoring", tabName = "monitoring", icon = icon("dashboard"), startExpanded = TRUE),
    #menuItem("Map", tabName = "map", icon = icon("map")),
    selectInput(inputId = 'selectfile', label = 'Select File', choice = list.files(logpath))
    # fileInput(inputId = 'selectfile', "Upload the file"),
    #textOutput('fileselected')
)

body <- dashboardBody(
    tags$head(
        tags$link(rel = "stylesheet", type = "text/css", href = "style.css")
    ),
    tags$head(
        tags$style(
            HTML('
                .skin-blue .main-header .logo {
                  background-color: #1D8CAF;
                }
                
                .skin-blue .main-header .logo:hover {
                  background-color: #1D8CAF;
                }
                
                .skin-blue .main-header .navbar{
                  background-color: #1D8CAF;
                }
                .skin-blue .left-side, .skin-blue .main-sidebar, .skin-blue .wrapper{
                  background-color: #0C5060;
                }
                
                img[src*="logo-white.svg"] {
                  max-height:52%;

                footer, footer a{
                  color: grey;
                }
                
                footer a:hover{
                  color: #1D8CAF;
                }
            ')
            )
        ),
    fluidPage(
        #tags$h1("Monitoring"),
        #tags$br(),
        wellPanel(
            fluidRow( 
                # Row 1
                tags$style(".small-box.bg-blue { background-color: #2C9CD6 !important; color: #fff !important; }"),
                valueBoxOutput("vbox1", width = 4),
                tags$style(".small-box.bg-yellow { background-color: #1D8CAF !important; color: #fff !important; }"),
                valueBoxOutput("vbox2", width = 4),
                tags$style(".small-box.bg-red { background-color: #3DD9D6 !important; color: #fff !important; }"),
                valueBoxOutput("vbox3", width = 4)
            )
        ),
        wellPanel(
            
            fluidRow(
                column(6, offset = 0, style='padding:5px;',
                       plotlyOutput(outputId = "plot1", height = 200)
                ),
                column(6, offset = 0, style='padding:5px;',
                       plotOutput(outputId = "plot2", height = 200)
                )
            ),
            fluidRow(
                column(6, offset = 0, style='padding:5px;',
                       plotOutput(outputId = "plot3", height = 200)
                ),
                column(6, offset = 0, style='padding:5px;',
                       plotOutput(outputId = "plot4", height = 200)
                )
            )
        ),
        # footer text
        tags$footer(HTML('Thank you for using <a href="https://lightmeter.io">Lightmeter</a>. &copy; 2020. <a href="https://lightmeter.io/privacy-policy/">Privacy Policy</a>.')),
        
        # sripts
        tags$script(HTML(
            "<!-- Matomo -->
                var _paq = window._paq || [];
                /* tracker methods like \"setCustomDimension\" should be called before \"trackPageView\" */
                _paq.push(['trackPageView']);
                _paq.push(['enableLinkTracking']);
                (function() {
                    var u=\"//matomo.lightmeter.io/\";
                    _paq.push(['setTrackerUrl', u+'matomo.php']);
                    _paq.push(['setSiteId', '1']);
                    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
                    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
                })();
            <!-- End Matomo Code -->"
        ))
    )
    
)

ui <- dashboardPage(
    title = "Lightmeter Tech Preview",
    header = dashboardHeader(title = tags$a(href='https://lightmeter.io/',
                                            tags$img(src='logo-white.svg'))),
    sidebar = sidebar,
    body = body
)

# Define server logic
server <- function(input, output) {
    
    data <- reactive({
            #require(data.table)
            # TODO use fread to make reading faster (1.5 - 2 times)
            parsed <- read_table2(paste0(logpath, input$selectfile, sep = ""), col_names = FALSE) %>%
            select(month = X1,
                   day = X2,
                   hms = X3,
                   smtp_node = X4,
                   postfix_job = X5,
                   status_info = X6,
                   to = X7,
                   relay = X8,
                   delay = X9,
                   delays = X10,
                   dsn = X11,
                   status = X12,
                   with = X13,
                   host = X14) %>%
            filter(str_detect(status, "^status="))
            return(parsed)
    })
    
    output$vbox1 <- renderValueBox({
        number_of_sent <- data() %>%
            filter(str_detect(status, "status=sent")) %>%
            count
        valueBox(
            width = 4,
            value = format(as.numeric(number_of_sent), big.mark=","),
            subtitle = "# Sent",
            color = "blue" #2C9CD6, #1D8CAF, #3DD9D6
        )
    })
    
    output$vbox2 <- renderValueBox({ 
        number_of_bounced <- data() %>%
            filter(str_detect(status, "status=bounced")) %>%
            count
        valueBox(
            width = 4,
            value = format(as.numeric(number_of_bounced), big.mark=","),
            subtitle = "# Bounced",
            color = "yellow"
        )
    })
    
    output$vbox3 <- renderValueBox({ 
        number_of_deferred <- data() %>%
            filter(str_detect(status, "status=deferred")) %>%
            count
        valueBox(
            width = 4,
            value = format(as.numeric(number_of_deferred), big.mark=","),
            subtitle = "# Deferred",
            color = "red"
        )
    })
    
    output$plot1 <- renderPlotly({
        t <- list(
            family = 'arial',
            size = 10,
            color = 'black')
        
        plot_ly(
            data(),
            labels = ~status,
            type = 'pie',
            marker = list(
                line = list(color = '#FFFFFF', width = .8))
        ) %>%
            layout(title = 'Delivery Overview', font=t, showlegend = FALSE) %>%
            layout(plot_bgcolor="#f5f5f5") %>% 
            layout(paper_bgcolor="#f5f5f5") %>%
            layout(colorway=c('#3DD9D6', '#2C9DD6', '#1D8CAF'))
    })
    
    output$plot2 <- renderPlot({
        top_recipients <- data() %>%
            mutate(to = str_extract(to, "([_a-z0-9-]+(\\.[_a-z0-9-]+)*@[a-z0-9-]+(\\.[a-z0-9-]+)*(\\.[a-z]{2,4}))")) %>%
            drop_na(to) %>%
            group_by(to) %>%
            tally(sort = TRUE, name = "count") %>%
            mutate(to = fct_rev(fct_inorder(to))) %>%
            top_n(n = 20)
        
        ggplot(top_recipients, aes(to, count)) + 
            geom_col(fill = "#FDECD5") + 
            coord_flip() + 
            theme1 +
            ggtitle("Most popular recipients")
    })
    
    output$plot3 <- renderPlot({
        top_recipients_bounced <- data() %>%
            filter(str_detect(status, "status=bounced")) %>%
            mutate(to = str_extract(to, "([_a-z0-9-]+(\\.[_a-z0-9-]+)*@[a-z0-9-]+(\\.[a-z0-9-]+)*(\\.[a-z]{2,4}))")) %>%
            drop_na(to) %>%
            group_by(to) %>%
            tally(sort = TRUE, name = "count") %>%
            mutate(to = fct_rev(fct_inorder(to))) %>%
            top_n(n = 3)
        
        ggplot(top_recipients_bounced, aes(to, count)) + 
            geom_col(fill = "#FDECD5") + 
            coord_flip() + 
            theme1 +
            ggtitle("Most bounced domains")
        
    })  
    
    output$plot4 <- renderPlot({
        deferred_mail_per_host <- data() %>%
            filter(str_detect(status, "status=deferred")) %>%
            mutate(host = str_extract(host, "(.*?(\\w+\\.\\w+))\\[")) %>%
            group_by(host) %>%
            tally(sort = TRUE, name = "count") %>%
            mutate(host = fct_rev(fct_inorder(host))) %>%
            top_n(n = 20)
        
        ggplot(deferred_mail_per_host, aes(host, count)) + 
            geom_col(fill = "#FDECD5") + 
            coord_flip() + 
            theme1 +
            ggtitle("Most deferring hosts")
    })
}

# Run the application 
shinyApp(ui = ui, server = server)
