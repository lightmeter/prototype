FROM debian:stable as builder

ENV IMAGE_DATE 2020-01-19

RUN apt-get update && apt-get install -y \
  r-base r-base-dev libxml2-dev libcurl4-openssl-dev libssl-dev file wget libstdc++6 binutils

RUN R -e 'install.packages(c("devtools", "readxl", "shiny", "shinydashboard", "readr", "tidyverse", "stringr", "plotly", "kableExtra"), repos="https://cran.rstudio.com")'

RUN find /usr/local -name '*.so' -exec strip --strip-all {} \;
RUN find /usr/local -name 'examples' -exec rm -rf {} +
RUN find /usr/local -name '*.h' -exec rm -rf {} \;
RUN find /usr/local -name '*.hpp' -exec rm -rf {} \;
RUN find /usr/local -name '*.cpp' -exec rm -rf {} \;

FROM debian:stable

COPY --from=builder /usr/local/lib/R /usr/local/lib/R

RUN apt-get update && apt-get install -y \
  && apt-get install -y r-base-core wget \
  && wget https://download3.rstudio.org/ubuntu-14.04/x86_64/shiny-server-1.5.12.933-amd64.deb \
  && dpkg -i shiny-server-1.5.12.933-amd64.deb \
  && rm -f shiny-server-1.5.12.933-amd64.deb \
  && apt-get purge -y wget gcc cpp libllvm7 libgl1-mesa-dri \
  && apt-get autoremove -y \
  && rm -rf /var/lib/apt/ /var/lib/dpkg/

RUN usermod -a -G adm shiny

EXPOSE 3838
VOLUME /logs

COPY app.R /lightmeter/
COPY libs.R /lightmeter/
COPY www /lightmeter/www

RUN mkdir -p /var/lib/shiny-server/
RUN chown -R shiny.shiny /var/lib/shiny-server/
COPY shiny-server.conf /etc/shiny-server/

USER shiny
WORKDIR /
CMD /usr/bin/shiny-server
